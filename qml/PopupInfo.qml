import QtQuick 2.2
import QtQuick.Controls 2.0

Item {
    id: rootItem
    property string title: ""
    property string text: ""
    property string color: "white"
    anchors.fill: parent

    visible: false
    z: parent.z + 1

    function show(title, text, color) {
        rootItem.title = title
        rootItem.text = text
        rootItem.color = color
        rootItem.visible = true
    }

    Rectangle {
        id: rootRectangle
        anchors.fill: parent
        color: rootItem.color

        Label {
            id: labelTitle
            x: parent.width / 2 - width / 2
            text: rootItem.title
            font.pixelSize: 80
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                rootItem.visible = false
            }
        }
        Label {
            id: contentText
            font.pixelSize: labelTitle.font.pixelSize * 0.9
            anchors.top: labelTitle.bottom
            anchors.bottom: closingInfoText.top
            //y: labelTitle.height * 1.1
            width: parent.width
            text: rootItem.text
            wrapMode: "WrapAtWordBoundaryOrAnywhere"
        }

        Label {
            id: closingInfoText
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            text: "Click anywhere to close"
        }
    }
}
