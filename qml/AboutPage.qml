import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.Layouts 1.3

Dialog {
            id: aboutDialog
            visible: false
            title: i18n.tr("About uText")

            Image {
                anchors.horizontalCenter: parent     
                source: '../assets/utext.png'
                fillMode: Image.PreserveAspectFit 
            }
            
Column {             

            Text {
            anchors.horizontalCenter: parent 
            color: "white"
            text: i18n.tr('Copyright (c) 2018-2021 by Zaunz Kenny <br><br>')
            }
            
             Text {
             anchors.horizontalCenter: parent 
             color: "white"
             text: i18n.tr("5805 Downloads ! Thank you ! <br> uText, very basic TextEditor Load, <br> Save and Edit every file on your Phone ! <br>")
            }
            
             Text {
             anchors.horizontalCenter: parent
             color: "white"
             text: i18n.tr("Please try it and give my feetback over <br> telegram (@BlueKenny) <br> Thanks in advance. <br>") 
            }
            
             Text {
             anchors.horizontalCenter: parent
             color: "white"
             text: i18n.tr("Wischlist of users, write me to ask more:") 
            }
            
             Text {
             anchors.horizontalCenter: parent
             color: "white"            
             text: i18n.tr("(3x) Option to add Tab key <br> (eventually as prediction if possible ?)") 
            }
            
             Text {
             anchors.horizontalCenter: parent
             color: "white"
             text: i18n.tr("(1x) Adding pdt2text to open PDF files") 
            } 
            
             Text {
             anchors.horizontalCenter: parent
             color: "white"
             text: i18n.tr("(1x) Support for TAB Key <br>") 
            }
       }            

            Button {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                color: UbuntuColors.orange
                text: i18n.tr('Donate')
                onClicked: Qt.openUrlExternally('https://de.liberapay.com/BlueKenny/donate')
            }

            Button {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                color: UbuntuColors.green
                text: i18n.tr('OK')
                onClicked: PopupUtils.close(aboutDialog)
            }
        }
