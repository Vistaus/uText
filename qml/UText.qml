import QtQuick 2.7
//import QtQuick.Controls 2.2
import Ubuntu.Components 1.3

Rectangle {
    id: root
    property string text: "Please choose a file\n\nHappy Editing ;)"
    property bool prediction: true
    z: 1
    border.width: 1
    radius: 20

    onPredictionChanged: {
        console.warn("prediction: " + prediction)
    }
/*
    Flickable {
        anchors.fill: parent
        TextArea.flickable: rootText

        ScrollBar.vertical: ScrollBar {
            active: true;
            //policy: ScrollBar.AlwaysOn
            //width: root.width / 35//vars.scrollbarWidth
        }*/

    TextArea {
        id: rootText
        anchors.fill: parent
        z: root.z + 1
        font.pixelSize: settingPage.textSize
        text: root.text
        selectByMouse: true
        enabled: mainWindow.noFileSelected ? false : true

        onTextChanged: {
            root.text = text
        }

        //wrapMode: "Wrap"

        inputMethodHints: root.prediction ? Qt.ImhNone : Qt.ImhNoPredictiveText


    }
    //}
}
