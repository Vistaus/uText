import QtQuick 2.9
import QtQuick.Controls 2.0
import io.thp.pyotherside 1.5
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import Qt.labs.settings 1.0

MainView {
    id: mainWindow
    applicationName: "utext.bluekenny"
    //width: units.gu(60)
    //height: units.gu(100)
    z: 1

    backgroundColor: settingPage.colorBackground

    anchorToKeyboard: true

    property list<ContentItem> importItems
    property var activeTransfer
    property bool directOpen: true
    property string textToOpen: ""

    property string file_manager_appID: "com.ubuntu.filemanager_filemanager" //<TODO> dynamic loading of this string

    property bool noFileSelected: true

    property int randAbstand : units.gu(4)


    Settings {
        id: saved
        property alias textSize: settingPage.textSize
        property alias randAbstand: mainWindow.randAbstand
        property alias prediction: textWriter.prediction
        //property int randAbstand
    }

    function getReadme(text) {
        console.warn("getReadme " + text)
    }


    onTextToOpenChanged: {
        console.warn("textToOpen: " + mainWindow.textToOpen)
        python.call("Main.openFile", [mainWindow.textToOpen, mainWindow.directOpen], function(content) {
            textWriter.text = content
        })
        python.call("Main.getFileName", [], function(filePath) {
            fileName.text = filePath
        })
    }

    PopupInfo{
        id: dialogInfo
        z: 3
    }

    Label {
        id: fileName
        text: i18n.tr("uText")
        x: mainWindow.width / 2 - width / 2
        y: randAbstand / 2 - height / 2
        font.pixelSize: settingPage.textSize
        color: settingPage.colorText
    }

   //TextArea {s
    UText {
        id: textWriter
        x: randAbstand
        width: mainWindow.width - 2 * x
        z: 1
        anchors {
            bottom: commandList.top
            top: parent.top
            margins: randAbstand
        }
        //font.pixelSize: 40
        //textFormat:TextEdit.RichText
        Component.onCompleted: {
            console.warn("UText Completed")
            if (mainWindow.textToOpen == "") {
                mainWindow.textToOpen = "new"
            //python.call("Main.getReadme", [], function() {})

            }
        }
    }

    SettingPage {
        id: settingPage
        width: textWriter.x
        z: 2
        initialWidth: randAbstand
    }

    CommandList {
        id: commandList
        //y: mainWindow.height - height
        width: mainWindow.width
        height: randAbstand * 1.8
        anchors {
            bottom: parent.bottom
        }
    }

    Terminal {
        id: terminal
    }

    ContentPeer {
        id: textSourceSingle
        contentType:ContentType.Text
        handler: ContentHandler.Destination
        selectionType: ContentTransfer.Single
    }
    ContentPeer {
        id: textLoadSingle
        appId: file_manager_appID
        contentType: ContentType.All
        handler: ContentHandler.Source
        selectionType: ContentTransfer.Single

    }
    /*
    ContentPeerPicker {
        id: textLoadSingleApp
        contentType:ContentType.Text // Documents unknow?
        handler: ContentHandler.Source
        visible: false
        z: 2

        onCancelPressed: {
            visible = false
        }
        onPeerSelected: {
            visible = false
            //console.warn(peer)
        }
    }*/

    ContentTransferHint {
        id: importHint
        anchors.fill: parent
        activeTransfer: mainWindow.activeTransfer
    }

    Connections {
        target: ContentHub
        onImportRequested: {
            terminal.visible = false
            console.warn("transfer.source: " + String(transfer.source))
            console.warn("transfer.destination: " + String(transfer.destination))
            console.warn("transfer.store: " + String(transfer.store))
            console.warn("transfer.item[0].url: " + String(transfer.items[0].url))
            mainWindow.directOpen = false
            mainWindow.textToOpen = String(transfer.items[0].url)

            mainWindow.noFileSelected = false
        }
    }

    PythonX {
        id: python

    }
}













