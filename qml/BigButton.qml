import QtQuick 2.2
import Ubuntu.Components 1.3

Rectangle {
    id: bigbutton

    property string bText
    property bool bEnabled: true
    signal clicked()

    border.width: 1
    radius: 35
    width: parent.height * 0.8
    height: parent.height * 0.8
    y: parent.height / 2 - height / 2
    color: settingPage.colorButton
    visible: enabled

    PropertyAnimation {
        id: animationClicked 
        target: bigbutton
        property: "border.width"
        to: 100
        duration: 150
        onStopped: {animationReleased.running = true}
    }
    PropertyAnimation {
        id: animationReleased
        target: bigbutton
        property: "border.width"
        to: 1
        duration: 150
    }

    Label {
        y: parent.height / 2 - height / 2
        x: parent.height / 2 - width / 2
        text: bText
        color: settingPage.colorButtonText
    }

    MouseArea {
        id: mouseArea
        anchors.fill : parent 
        onClicked: {
            if (bEnabled == true) {
                animationClicked.running = true 
                bigbutton.clicked()
            }
        }
    }
}











