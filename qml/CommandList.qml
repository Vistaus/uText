import QtQuick 2.9
import QtQuick.Controls 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Flickable {//Rectangle {
    width: units.gu(50)
    height : units.gu(80)
    //border.width : 1

    contentWidth: switchWordPrediction.x + switchWordPrediction.width * 2; //contentHeight: eight

    ScrollBar.horizontal: ScrollBar {
        active: true//elItem.scrollbarVisible;
        //policy: ScrollBar.AlwaysOn
        width: 50//elItem.scrollbarWidth
    }

    BigButton {
        id: bigBSave
        x: units.gu(5)
        bText: i18n.tr("Save")
        enabled: mainWindow.noFileSelected ? false : true
        onClicked: {
            Qt.inputMethod.commit();
            console.warn(textWriter.text)
            python.call("Main.saveFile",[textWriter.text], function(sucess) {
                if (sucess == true) {
                    dialogInfo.show(qsTr("Saved"), qsTr("File sucessfull saved :)"), "lightgreen")
                }
               if (sucess == false) {
                   dialogInfo.show(qsTr("Error !"), qsTr("Error, file not saved :("), "red")
               }

            })
        }
    }
    BigButton {
        id: bigBLoad
        x: 1.5 * bigBSave.x + bigBSave.width
        bText: i18n.tr("Load")

        onClicked: {
            //textLoadSingleApp.visible = true
            activeTransfer = textLoadSingle.request()
            //dialogSaveLoad.load()
        }
    }/*
    BigButton {
        id: bigBRun
        x: 2 * bigBSave.x + 2 * bigBSave.width
        bText: "Run"
        enabled: false
        onClicked: {
             terminal.visible = true
             python.call("Main.terminal", [], function() {})
        }
    }*/

    BigButton {
        id: bigBHelp
        x: 1.8 * bigBSave.x + 1.8 * bigBSave.width
        bText: i18n.tr("Help")
        onClicked: {
            Qt.openUrlExternally("https://forums.ubports.com/topic/2602/utext-help-devel")
        }
    }
    
    BigButton {
        id: bigBAbout
        x: 2.4 * bigBSave.x + 2.4 * bigBSave.width
        bText: i18n.tr("About")
        onClicked: {
            PopupUtils.open(Qt.resolvedUrl("AboutPage.qml"))
        }
    }    

    Switch {
        id: switchWordPrediction
        Label {
            id: labelSwitch
            text: i18n.tr("Word Prediction")
            anchors.top: parent.bottom
            color: settingPage.colorText
        }
        y: parent.height/2 - height/2 - labelSwitch.height/2
        checked: textWriter.prediction//true // saved status
        x: 3 * bigBSave.x + 3 * bigBHelp.width

        enabled: mainWindow.noFileSelected ? false : true
        onCheckedChanged: {
            textWriter.prediction = checked
        }
    }

    PythonX{id:python}
}









