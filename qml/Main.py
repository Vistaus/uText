#!/usr/bin/env python3

import os
import sys

try: import pyotherside
except: True

file = ""
config_dir = "/home/phablet/.local/share/utext.bluekenny/"
if not os.path.exists(config_dir): os.mkdir(config_dir)

def getReadme():
    if os.path.exists("new"):
        readme = open("new", "r").read()
        os.remove("new")
        pyotherside.send("getNew", new)

def getFileName():
    return file

def saveFile(fileContent):
    global file
    fileContent = str(fileContent)
    print("Save: " + str(fileContent))
    try:
        open(file, "w").write(fileContent)
        return True
    except: return False 

def openFile(filePath, directOpen):
    global file

    filePath = str(filePath)
    filePath = filePath.replace("file://", "")
    print("filePath: " + filePath)
    
    directOpen = bool(directOpen)

    if not directOpen:
        logFiles = []
        for eachLog in os.listdir("/home/phablet/.cache/upstart/"):
            if "filemanager" in eachLog and not ".gz" in eachLog: logFiles.append(eachLog)
        print("logFiles: " + str(logFiles))
        linesToCheck = open("/home/phablet/.cache/upstart/" + logFiles[-1], "r").readlines()

        while True:
            for line in reversed(linesToCheck):
                line = line.rstrip()
                #print("Line to check: " + str(line))
                if "qml: resolveContentType for file" in line or "accept file selector" in line:
                    print("line: " + str(line))
                    filePath = line.split("file://")[-1]
                    break 
            break 


    content = ""
    if os.path.exists(filePath):
        file = open(filePath, "r")
        content = file.read()

        print("content: " + str(content))

    else: print("path existiert nicht: " + str(filePath))


    file = filePath

    return content


def terminal():
    global file
    terminalOutput = "uText Terminal \n"

    terminalOutput +="Work in progress ;) \n"
    print("Starting terminal")
    dir = file.split("/")
    del dir[-1]
    dir = "/".join(dir)

    os.chdir(dir)
    os.system("chmod +x " + file)
    terminalOutput += os.popen(file, "r", 1).read()

    terminalOutput += "\n Command Complete"
    pyotherside.send("terminalOutput", terminalOutput)
