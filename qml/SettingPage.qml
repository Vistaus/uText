import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Window 2.0
import Ubuntu.Components 1.3

import Ubuntu.Components.Pickers 1.3

Item {
    id: settings
    height: Screen.height; width: Screen.width
    property int initialWidth: 20
    property bool settingsOpen: false
    property string colorBackground: "lightblue"
    property string colorButton: "grey"
    property string colorButtonText: "white"
    property string colorText: "black"

    property int textSize: 40

    onSettingsOpenChanged: {
        console.warn("Theme: " + Theme.name)
    }

    Rectangle {
        id: rectangleSettings
        color: colorBackground
        width: settingsOpen ? mainWindow.width : initialWidth
        height: parent.height
        //z: 2

        Label {
            id: settingsSwipeText
            text: settingsOpen ? "Swipe to close Settings" : "Swipe to open Settings"
            rotation: -90
            y: rectangleSettings.height / 2 - height
            anchors.right: rectangleSettings.right
            color: colorText
            //anchors.fill: parent
            width: height
        }

        MouseArea {
            id: mouseAreaSettingSwipe
            x: settingsOpen ? mainWindow.width - 1.5*initialWidth : 0
            width: settingsOpen ? 2*initialWidth : initialWidth
            height: mainWindow.height

            onPressed: {
                console.warn("pos: " + mouseX)
            }
            onMouseXChanged: {
                if (pressedButtons == 1) {
                    //console.warn("Move")
                    if (settingsOpen == true) {
                        rectangleSettings.width = mouseX + (mainWindow.width - initialWidth)
                    } else {
                        rectangleSettings.width = mouseX
                    }
                }
            }
            onReleased: {
                if (rectangleSettings.width > mainWindow.width / 2) {
                    settingsOpen = true
                    rectangleSettings.width = mainWindow.width
                } else {
                    settingsOpen = false
                    rectangleSettings.width = initialWidth
                }
            }
        }

        Flickable {
            id: settingFlickable
            height: rectangleSettings.height //settingsOpen ? rectangleSettings.height : 0
            width: rectangleSettings.width - mouseAreaSettingSwipe.width //settingsOpen ? rectangleSettings.width - mouseAreaSettingSwipe.width : 0
            contentHeight: 2*height
            contentWidth: width
            boundsBehavior: Flickable.StopAtBounds
            visible: settingsOpen

            Rectangle {
                anchors.fill: parent
                color: colorBackground

                Label {
                    id: titleFontSize
                    text: "Font Size: " + settings.textSize
                    font.pixelSize: settings.textSize
                    color: colorText
                }
                Slider {
                    y: titleFontSize.y
                    anchors.left: titleFontSize.right
                    anchors.right: parent.right
                    minimumValue: settings.textSize - 5.0
                    maximumValue: settings.textSize + 5.0
                    stepSize: 1.0
                    value: settings.textSize
                    onValueChanged: {
                        console.warn("Slider FondSize: " + value)
                        settings.textSize = value
                    }
                }
                Label {
                    id: titleBorderSize
                    text: "Border Size: " + mainWindow.randAbstand
                    font.pixelSize: settings.textSize
                    color: colorText
                    anchors.top: titleFontSize.bottom
                }
                Slider {
                    y: titleBorderSize.y
                    anchors.left: titleBorderSize.right
                    anchors.right: parent.right
                    minimumValue: mainWindow.randAbstand - 2.0
                    maximumValue: mainWindow.randAbstand + 2.0
                    stepSize: 1.0
                    value: mainWindow.randAbstand
                    onValueChanged: {
                        console.warn("Slider BorderSize: " + value)
                        mainWindow.randAbstand = value
                    }
                }


                ListView {
                    id: listThemes
                    anchors {
                        left: parent.left
                        //right: parent.right
                        top: titleBorderSize.bottom
                    }
                    width: mainWindow.width - mouseAreaSettingSwipe.width
                    height: width
                    orientation: ListView.Horizontal

                    clip: true

                    ListModel {
                        id: listThemesModel
                        ListElement {
                            sourceImage: "../themes/blue-black.png"
                        }
                        ListElement {
                            sourceImage: "../themes/blue-black.png"
                        }
                    }

                    Component {
                        id: listThemesDelegate
                        Item {
                            width: listThemes.width * 0.8
                            height: width
                            Image {
                                source: sourceImage
                                //width: listThemes.width //- 10
                                //height: listThemes.height //- 10
                                anchors.fill: parent
                                fillMode: Image.PreserveAspectFit
                            }
                        }
                    }

                    model: listThemesModel
                    delegate: listThemesDelegate

                }



/*
                Label {
                    text: "WORK in Progress :p"
                    rotation: -45
                    font.pixelSize: settings.textSize
                    //y: titleFontSize.height * 8
                    anchors.top: listThemes.bottom
                    color: colorText

                }*/
            }
        }
    }
}
