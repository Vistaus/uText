import QtQuick 2.2
import Ubuntu.Components 1.3

Rectangle {
    id: root

   anchors.fill : parent

    visible:false

    function terminalOutput(output) {
        terminalArea.text = output
    }
    Button {
        id: buttonBack
        text: i18n.tr("Back")

        onClicked: { root.visible = false }
    }

    TextArea {
        id: terminalArea
        text: i18n.tr("TERMINAL")
        anchors {
            top: buttonBack.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }

    PythonX {id: python}
}











